-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2019 at 11:30 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `order` int(11) NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `detail` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `amphur`
--

CREATE TABLE `amphur` (
  `AMPHUR_ID` int(5) NOT NULL,
  `AMPHUR_CODE` varchar(4) NOT NULL,
  `AMPHUR_NAME` varchar(150) NOT NULL,
  `PROVINCE_ID` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `barber_branch`
--

CREATE TABLE `barber_branch` (
  `id` int(100) NOT NULL,
  `barber_id` int(100) NOT NULL,
  `branch_id` int(10) NOT NULL,
  `level` varchar(20) NOT NULL,
  `status_soc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `branch_id` int(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `manager_id` int(10) NOT NULL,
  `status` varchar(100) NOT NULL,
  `detail` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `Province_name` varchar(50) DEFAULT NULL,
  `amphur_name` varchar(50) DEFAULT NULL,
  `district_name` varchar(50) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `map_img` varchar(100) DEFAULT NULL,
  `location` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `branch_img`
--

CREATE TABLE `branch_img` (
  `bar_img_id` int(10) NOT NULL,
  `barber_id` int(100) NOT NULL,
  `img_1` varchar(50) DEFAULT NULL,
  `caption_1` varchar(50) DEFAULT NULL,
  `img_2` varchar(50) DEFAULT NULL,
  `caption_2` varchar(50) DEFAULT NULL,
  `img_3` varchar(50) DEFAULT NULL,
  `caption_3` varchar(50) DEFAULT NULL,
  `img_4` varchar(50) DEFAULT NULL,
  `caption_4` varchar(50) DEFAULT NULL,
  `img_5` varchar(50) DEFAULT NULL,
  `caption_5` varchar(50) DEFAULT NULL,
  `img_6` varchar(50) DEFAULT NULL,
  `caption_6` varchar(50) DEFAULT NULL,
  `img_7` varchar(50) DEFAULT NULL,
  `caption_7` varchar(50) DEFAULT NULL,
  `img_8` varchar(50) DEFAULT NULL,
  `caption_8` varchar(50) DEFAULT NULL,
  `img_9` varchar(50) DEFAULT NULL,
  `caption_9` varchar(50) DEFAULT NULL,
  `img_10` varchar(50) DEFAULT NULL,
  `caption_10` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `check_work`
--

CREATE TABLE `check_work` (
  `check_work_id` int(10) NOT NULL,
  `barber_id` int(10) NOT NULL,
  `start_work` datetime NOT NULL,
  `finish_work` datetime NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `DISTRICT_ID` int(5) NOT NULL,
  `DISTRICT_CODE` varchar(6) NOT NULL,
  `DISTRICT_NAME` varchar(150) NOT NULL,
  `AMPHUR_ID` int(5) NOT NULL,
  `PROVINCE_ID` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `expenses_bar`
--

CREATE TABLE `expenses_bar` (
  `ex_bar_id` int(10) NOT NULL,
  `type` varchar(50) NOT NULL,
  `barber_id` int(10) NOT NULL,
  `money` int(255) NOT NULL,
  `time_stamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `information`
--

CREATE TABLE `information` (
  `in_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `e-mail` varchar(30) NOT NULL,
  `title` varchar(100) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `question_dt` datetime NOT NULL,
  `res_id` varchar(10) DEFAULT NULL,
  `answer` varchar(100) DEFAULT NULL,
  `answer_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE `leaves` (
  `leave_id` int(10) NOT NULL,
  `type_id` int(10) NOT NULL,
  `detail` varchar(50) DEFAULT NULL,
  `barber_id` int(10) NOT NULL,
  `date_start` datetime NOT NULL,
  `date_stop` datetime NOT NULL,
  `total_day` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `leave_type`
--

CREATE TABLE `leave_type` (
  `leave_type_id` int(10) NOT NULL,
  `vac_type` varchar(50) NOT NULL,
  `day_max` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `level_barber`
--

CREATE TABLE `level_barber` (
  `lev_babr_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `percent` int(10) NOT NULL,
  `salary` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `list_menu`
--

CREATE TABLE `list_menu` (
  `list_menu1_id` int(10) NOT NULL,
  `menu_id` int(10) NOT NULL,
  `list_menu_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `list_menu`
--

INSERT INTO `list_menu` (`list_menu1_id`, `menu_id`, `list_menu_name`) VALUES
(1, 3, 'ผู้จัดการสาขา'),
(2, 3, 'ช่างซอย'),
(3, 3, 'ลูกค้า'),
(4, 3, 'ช่างสระไดร์');

-- --------------------------------------------------------

--
-- Table structure for table `list_menu0`
--

CREATE TABLE `list_menu0` (
  `menu_id` int(10) NOT NULL,
  `menu_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `list_menu0`
--

INSERT INTO `list_menu0` (`menu_id`, `menu_name`) VALUES
(2, 'ติดต่อสอบถาม'),
(3, 'เพิ่มสมาชิก'),
(4, 'จัดการสมาชิก'),
(5, 'ดูสมาชิก'),
(6, 'ดูการประเมิน'),
(7, 'ตั้งค่า'),
(8, 'สรุปยกเลิกการจอง'),
(9, 'สรุปงานช่าง'),
(10, 'สรุปเงินช่าง'),
(11, 'สรุปเข้างาน'),
(12, 'รายจ่ายช่าง'),
(13, 'สรุปเปลี่ยนช่าง'),
(14, 'จัดการคิว'),
(15, 'ยืนยันการมาใช้บริการ'),
(16, 'บันทึกเวลาเริ่มให้บริการ'),
(17, 'บันทึกเวลา/ชำระค่าบริการ'),
(18, 'ลา'),
(19, 'แก้ไขการลา'),
(20, 'เข้างาน'),
(21, 'แก้ไขเวลาเข้างาน'),
(22, 'ความพึงพอใจ'),
(23, 'โปรไฟล์'),
(24, 'แก้ไขโปรไฟล์'),
(25, 'สถานะการจอง'),
(26, 'ประวัติการใช้บริการ'),
(27, 'เปลี่ยนรหัสผ่าน'),
(28, 'คิวงาน'),
(29, 'ออกจากระบบ');

-- --------------------------------------------------------

--
-- Table structure for table `list_menu2`
--

CREATE TABLE `list_menu2` (
  `list_menu2_id` int(10) NOT NULL,
  `menu_id` int(10) NOT NULL,
  `list_menu2_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `list_menu2`
--

INSERT INTO `list_menu2` (`list_menu2_id`, `menu_id`, `list_menu2_name`) VALUES
(1, 7, 'เวลาเข้างาน'),
(2, 7, 'ประกันร้าน'),
(3, 7, 'ประกันสังคม'),
(4, 7, 'วันหยุด/ลา'),
(5, 7, 'จำนวนได้สิทธิ์ฟรี'),
(6, 7, 'ตั้งค่าเงินเดือน/%');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(10) NOT NULL,
  `menu_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_name`) VALUES
(1, 'ประชาสัมพันธ์'),
(2, 'บริการ'),
(3, 'ช่าง'),
(4, 'สาขา'),
(5, 'จองคิว'),
(6, 'เข้าระบบ');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `create_dt` datetime NOT NULL,
  `detail` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news_img`
--

CREATE TABLE `news_img` (
  `news_img_id` int(10) NOT NULL,
  `news_id` int(10) NOT NULL,
  `img_1` varchar(50) DEFAULT NULL,
  `img_2` varchar(50) DEFAULT NULL,
  `img_3` varchar(50) DEFAULT NULL,
  `img_4` varchar(50) DEFAULT NULL,
  `img_5` varchar(50) DEFAULT NULL,
  `img_6` varchar(50) DEFAULT NULL,
  `img_7` varchar(50) DEFAULT NULL,
  `img_8` varchar(50) DEFAULT NULL,
  `img_9` varchar(50) DEFAULT NULL,
  `img_10` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `point`
--

CREATE TABLE `point` (
  `point_id` int(100) NOT NULL,
  `customer_id` int(100) NOT NULL,
  `service_id` int(100) NOT NULL,
  `point_customer` int(100) NOT NULL,
  `update_dt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE `province` (
  `PROVINCE_ID` int(5) NOT NULL,
  `PROVINCE_CODE` varchar(2) NOT NULL,
  `PROVINCE_NAME` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `queue`
--

CREATE TABLE `queue` (
  `queue_id` int(10) NOT NULL,
  `branch_id` int(10) NOT NULL,
  `customer_id` int(100) NOT NULL,
  `total_time` varchar(50) NOT NULL,
  `total_money` varchar(50) NOT NULL,
  `queue_dt` datetime NOT NULL,
  `status_confirm` varchar(20) NOT NULL,
  `status_coome` varchar(20) NOT NULL,
  `start_real` datetime NOT NULL,
  `finish_real` datetime NOT NULL,
  `status_pay` varchar(20) NOT NULL,
  `feedback_branch` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `queue_detail`
--

CREATE TABLE `queue_detail` (
  `que_det_id` int(10) NOT NULL,
  `queue_id` int(10) DEFAULT NULL,
  `service_id` int(10) DEFAULT NULL,
  `barber_id` int(100) DEFAULT NULL,
  `add_money` int(50) DEFAULT NULL,
  `add_time` int(10) DEFAULT NULL,
  `detail` varchar(50) DEFAULT NULL,
  `start_dt` datetime NOT NULL,
  `finish_dt` datetime NOT NULL,
  `status_point` varchar(10) DEFAULT NULL,
  `feedback_service` int(10) DEFAULT NULL,
  `feedback_barber` int(10) DEFAULT NULL,
  `change_ber` int(10) DEFAULT NULL,
  `detail_hange_ber` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `service` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `take_time` varchar(10) NOT NULL,
  `detail` varchar(50) DEFAULT NULL,
  `service_charge` int(50) NOT NULL,
  `point` int(100) NOT NULL,
  `type_bar` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `service_img`
--

CREATE TABLE `service_img` (
  `service_img_id` int(10) NOT NULL,
  `service_id` int(10) NOT NULL,
  `img_1` varchar(50) DEFAULT NULL,
  `caption_1` varchar(50) DEFAULT NULL,
  `img_2` varchar(50) DEFAULT NULL,
  `caption_2` varchar(50) DEFAULT NULL,
  `img_3` varchar(50) DEFAULT NULL,
  `caption_3` varchar(50) DEFAULT NULL,
  `img_4` varchar(50) DEFAULT NULL,
  `caption_4` varchar(50) DEFAULT NULL,
  `img_5` varchar(50) DEFAULT NULL,
  `caption_5` varchar(50) DEFAULT NULL,
  `img_6` varchar(50) DEFAULT NULL,
  `caption_6` varchar(50) DEFAULT NULL,
  `img_7` varchar(50) DEFAULT NULL,
  `caption_7` varchar(50) DEFAULT NULL,
  `img_8` varchar(50) DEFAULT NULL,
  `caption_8` varchar(50) DEFAULT NULL,
  `img_9` varchar(50) DEFAULT NULL,
  `caption_9` varchar(50) DEFAULT NULL,
  `img_10` varchar(50) DEFAULT NULL,
  `caption_10` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `setting_bar`
--

CREATE TABLE `setting_bar` (
  `chack_in` datetime NOT NULL,
  `time_s` int(10) NOT NULL,
  `social_security` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `skills_id` int(10) NOT NULL,
  `barber_id` int(10) NOT NULL,
  `service_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userid` int(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` varchar(20) NOT NULL,
  `status` varchar(50) NOT NULL,
  `register_dt` datetime NOT NULL,
  `reg_approve` varchar(50) NOT NULL,
  `img_profile` varchar(50) DEFAULT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `nickname` varchar(10) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `phone_number` varchar(10) DEFAULT NULL,
  `gender` varchar(1) NOT NULL,
  `address` varchar(100) DEFAULT NULL,
  `PROVINCE_NAME` varchar(20) DEFAULT NULL,
  `AMPHUR_NAME` varchar(20) DEFAULT NULL,
  `DISTRICT_NAME` varchar(20) DEFAULT NULL,
  `ZIPCODE` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userid`, `username`, `password`, `level`, `status`, `register_dt`, `reg_approve`, `img_profile`, `firstname`, `lastname`, `nickname`, `date_of_birth`, `phone_number`, `gender`, `address`, `PROVINCE_NAME`, `AMPHUR_NAME`, `DISTRICT_NAME`, `ZIPCODE`) VALUES
(1, 'bnmil022', '09092537', '1', '1', '0000-00-00 00:00:00', 'd', '1', 'beer', 'balankd', 'beer', NULL, '0898061748', '1', 'dsa', 'd', 'dd', 'd', 'w');

-- --------------------------------------------------------

--
-- Table structure for table `zipcode`
--

CREATE TABLE `zipcode` (
  `ZIPCODE_ID` int(5) NOT NULL,
  `DISTRICT_CODE` varchar(100) NOT NULL,
  `PROVINCE_ID` varchar(100) NOT NULL,
  `AMPHUR_ID` varchar(100) NOT NULL,
  `ZIPCODE` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `amphur`
--
ALTER TABLE `amphur`
  ADD PRIMARY KEY (`AMPHUR_ID`);

--
-- Indexes for table `barber_branch`
--
ALTER TABLE `barber_branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `branch_img`
--
ALTER TABLE `branch_img`
  ADD PRIMARY KEY (`bar_img_id`);

--
-- Indexes for table `check_work`
--
ALTER TABLE `check_work`
  ADD PRIMARY KEY (`check_work_id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`DISTRICT_ID`);

--
-- Indexes for table `expenses_bar`
--
ALTER TABLE `expenses_bar`
  ADD PRIMARY KEY (`ex_bar_id`);

--
-- Indexes for table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`in_id`);

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`leave_id`);

--
-- Indexes for table `leave_type`
--
ALTER TABLE `leave_type`
  ADD PRIMARY KEY (`leave_type_id`);

--
-- Indexes for table `level_barber`
--
ALTER TABLE `level_barber`
  ADD PRIMARY KEY (`lev_babr_id`);

--
-- Indexes for table `list_menu`
--
ALTER TABLE `list_menu`
  ADD PRIMARY KEY (`list_menu1_id`);

--
-- Indexes for table `list_menu0`
--
ALTER TABLE `list_menu0`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `list_menu2`
--
ALTER TABLE `list_menu2`
  ADD PRIMARY KEY (`list_menu2_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `news_img`
--
ALTER TABLE `news_img`
  ADD PRIMARY KEY (`news_img_id`);

--
-- Indexes for table `point`
--
ALTER TABLE `point`
  ADD PRIMARY KEY (`point_id`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`PROVINCE_ID`);

--
-- Indexes for table `queue`
--
ALTER TABLE `queue`
  ADD PRIMARY KEY (`queue_id`);

--
-- Indexes for table `queue_detail`
--
ALTER TABLE `queue_detail`
  ADD PRIMARY KEY (`que_det_id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`service`);

--
-- Indexes for table `service_img`
--
ALTER TABLE `service_img`
  ADD PRIMARY KEY (`service_img_id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`skills_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `amphur`
--
ALTER TABLE `amphur`
  MODIFY `AMPHUR_ID` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `barber_branch`
--
ALTER TABLE `barber_branch`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `branch_id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `branch_img`
--
ALTER TABLE `branch_img`
  MODIFY `bar_img_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `check_work`
--
ALTER TABLE `check_work`
  MODIFY `check_work_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `DISTRICT_ID` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenses_bar`
--
ALTER TABLE `expenses_bar`
  MODIFY `ex_bar_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `information`
--
ALTER TABLE `information`
  MODIFY `in_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `leave_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `leave_type`
--
ALTER TABLE `leave_type`
  MODIFY `leave_type_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `level_barber`
--
ALTER TABLE `level_barber`
  MODIFY `lev_babr_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `list_menu`
--
ALTER TABLE `list_menu`
  MODIFY `list_menu1_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `list_menu0`
--
ALTER TABLE `list_menu0`
  MODIFY `menu_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `list_menu2`
--
ALTER TABLE `list_menu2`
  MODIFY `list_menu2_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news_img`
--
ALTER TABLE `news_img`
  MODIFY `news_img_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `point`
--
ALTER TABLE `point`
  MODIFY `point_id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `PROVINCE_ID` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `queue`
--
ALTER TABLE `queue`
  MODIFY `queue_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `queue_detail`
--
ALTER TABLE `queue_detail`
  MODIFY `que_det_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `service` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `service_img`
--
ALTER TABLE `service_img`
  MODIFY `service_img_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `skills_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userid` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
